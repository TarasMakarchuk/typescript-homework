export function unmarkHeart(): void {
  const heartIcon = document.getElementsByClassName('bi-heart-fill');
  Array.from(heartIcon).forEach(item => item.setAttribute('fill', '#ff000078'));
}

export const clearFavoriteListMovies = (): void => {
  const favoriteMoviesContainer = <HTMLElement>document.getElementById('favorite-movies');
  favoriteMoviesContainer.innerHTML = '';
};
