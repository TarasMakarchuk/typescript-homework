const KEY_LOCAL_STORAGE = `favorite_movie_id`;

export function addMovieLocalStorage(id: number): void {
  const items: Array<number> = JSON.parse(<string>localStorage.getItem(KEY_LOCAL_STORAGE)) || [];
  items.push(id);
  localStorage.setItem(KEY_LOCAL_STORAGE, JSON.stringify([...Array.from(new Set(items))]));
}

export function getMoviesLocalStorage(): Array<number> {
  return JSON.parse(<string>localStorage.getItem(KEY_LOCAL_STORAGE));
}

export function deleteMoviesLocalStorage(id: number): void {
  const items = JSON.parse(<string>localStorage.getItem(KEY_LOCAL_STORAGE));
  const filtered = items.filter((item: number, index: number) => items[index] !== id);
  localStorage.setItem(KEY_LOCAL_STORAGE, JSON.stringify(filtered));
}
