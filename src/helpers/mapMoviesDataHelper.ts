import { MoviesResponseData } from '../interfaces/IMovies';
import { moviesData } from '../store/MoviesData';
import { favoriteMoviesData } from '../store/FavoritesData';

export const mapMoviesData = async (results: Array<any>): Promise<void> => {
  await results.map((item: any) => {
    const moviesResponseData = new MoviesResponseData(item);
    moviesData.setMoviesData(moviesResponseData);
  });
};

export const mapFavoritesMoviesData = async (results: Array<any>): Promise<void> => {
  await results.map((item: any) => {
    const moviesResponseData = new MoviesResponseData(item);
    favoriteMoviesData.setFavoriteMoviesData(moviesResponseData);
  });
};
