import { MoviesResponseData } from '../interfaces/IMovies';

class MoviesData {
  private dataMovies: Array<any> = [];

  getMoviesData(): Array<MoviesResponseData> {
    return this.dataMovies;
  }

  setMoviesData(value: MoviesResponseData) {
    this.dataMovies.push({
      ...value,
    });
  }

  clearMoviesData = (): void => {
    this.dataMovies = [];
  };
}

export const moviesData = new MoviesData();
