import { MoviesResponseData } from '../interfaces/IMovies';

class FavoriteMoviesData {
  private dataMovies: Array<any> = [];

  getFavoriteMoviesData(): Array<MoviesResponseData> {
    return this.dataMovies;
  }

  setFavoriteMoviesData(value: MoviesResponseData) {
    this.dataMovies.push({
      ...value,
    });
  }

  clearFavoritesMoviesData = (): void => {
    this.dataMovies = [];
  };
}

export const favoriteMoviesData = new FavoriteMoviesData();
