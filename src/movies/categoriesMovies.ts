import { API_KEY, API_PATH, DEFAULT_POSTER_URL, POSTER_URL } from '../consts/consts';
import { MoviesCategory } from '../enums/enums';
import { moviesData } from '../store/MoviesData';
import { mapMoviesData } from '../helpers/mapMoviesDataHelper';
import { getMoviesLocalStorage } from '../helpers/localStorageHelper';
import { clearFavoriteListMovies, unmarkHeart } from '../helpers/domHelpers';

export async function categoriesMovies(): Promise<void> {

  let pageCount = 1;
  const URL_POPULAR = `${API_PATH}/movie/popular?api_key=${API_KEY}&language=en-US&page=${pageCount}`;
  const URL_UPCOMING = `${API_PATH}/movie/upcoming?api_key=${API_KEY}&language=en-US&page=${pageCount}`;
  const URL_TOP_RATED = `${API_PATH}/movie/top_rated?api_key=${API_KEY}&language=en-US&page=${pageCount}`;

  const groupRadioInput = <HTMLCollection>document.getElementsByClassName('btn-check');
  const cardContainerDescriptionList = <HTMLCollection>document.getElementsByClassName('card-body');
  const postersList = <HTMLCollection>document.getElementsByTagName('img');
  const overview = <HTMLCollection>document.getElementsByClassName('card-text truncate');
  const releaseDate = <HTMLCollection>document.getElementsByClassName('text-muted');

  const heartIcon = document.getElementsByClassName('bi-heart-fill');

  const loadMoreButton = <HTMLButtonElement>document.getElementById('load-more');

  const randomMovieContainer = <HTMLDivElement>document.getElementById('random-movie');
  const randomMovieTitle = <HTMLDivElement>document.getElementById('random-movie-name');
  const randomMovieTextContainer = <HTMLDivElement>document.getElementById('random-movie-description');
  const randomImage = <HTMLCollection>document.getElementsByClassName('random-image');

  const displayFilms = (url: string): void => {
    clearFavoriteListMovies();
    clearRandomMovie();
    searchMovies(url);
    clearTitle();
    moviesData.clearMoviesData();
    unmarkHeart();
  };

  groupRadioInput[MoviesCategory.Popular].addEventListener('change', () => displayFilms(URL_POPULAR));
  groupRadioInput[MoviesCategory.Upcoming].addEventListener('change', () => displayFilms(URL_UPCOMING));
  groupRadioInput[MoviesCategory.TopRated].addEventListener('change', () => displayFilms(URL_TOP_RATED));

  window.addEventListener('load', () => {
    displayFilms(URL_POPULAR);
  });

  const searchMovies = async (URL: string): Promise<void> => {
    const options = {
      method: 'GET',
    };
    await fetch(URL, options)
        .then(response => response.json())
        .then(data => {
          const { results } = data;
          if (results !== undefined) {
            mapMoviesData(results);
            showMovieInformation(moviesData.getMoviesData());
            displayRandomMovie(moviesData.getMoviesData());
          }
        })
        .catch(error => {
          console.error(error);
        });
  };

  const displayRandomMovie = (arrayDataMovies: Array<any>): void => {
    const randomElement = arrayDataMovies[Math.floor(Math.random() * arrayDataMovies.length)];

    const img = <HTMLImageElement>document.createElement('img');
    img.setAttribute('src', `${POSTER_URL}${randomElement.backdrop_path}`);
    img.setAttribute('class', 'random-image');
    randomMovieTextContainer.innerHTML = randomElement.overview;
    randomMovieTitle.innerHTML = randomElement.title;
    randomMovieContainer.appendChild(img);
  };

  const clearRandomMovie = () => {
    randomMovieTextContainer.innerHTML = '';
    randomMovieTitle.innerHTML = '';
    for (let i = 0; i < randomImage.length; i++) {
      randomMovieContainer.removeChild(randomImage[i]);
    }
  }

  const showMovieInformation = (arrayDataMovies: Array<any>): void => {
    const favoriteMoviesId: Array<number> = getMoviesLocalStorage();
    for (let i = 0; i < arrayDataMovies.length; i++) {
      const title = document.createElement('h5');
      title.setAttribute('style', 'text-align: center');
      title.setAttribute('class', 'movie-title');

      if (favoriteMoviesId !== null) {
        const favorite = favoriteMoviesId.filter(item => item === arrayDataMovies[i].id);
        if (favorite.length !== 0) {
          heartIcon[i].setAttribute('fill', 'red');
        }
      }

      if (arrayDataMovies[i].poster_path === null || arrayDataMovies[i].overview === null || arrayDataMovies[i].release_date === null) {
        postersList[i].setAttribute('src', DEFAULT_POSTER_URL);
        overview[i].innerHTML = 'No overview';
        releaseDate[i].innerHTML = 'No data release';
      } else {
        postersList[i].setAttribute('src', POSTER_URL + arrayDataMovies[i].poster_path);
        overview[i].innerHTML = arrayDataMovies[i].overview;
        releaseDate[i].innerHTML = arrayDataMovies[i].release_date;
      }

      title.innerHTML = arrayDataMovies[i].title;
      cardContainerDescriptionList[i].appendChild(title);
    }
  };

  const clearTitle = (): void => {
    const clearTitle = document.getElementsByTagName('h5');
    for (let i = 0; i < clearTitle.length; i++) {
      clearTitle[i].innerHTML = '';
    }
  };

  loadMoreButton.addEventListener('click', function() {
    pageCount += 1;
  });
}
