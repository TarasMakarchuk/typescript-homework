import { API_KEY, API_PATH, DEFAULT_POSTER_URL, POSTER_URL } from '../consts/consts';
import { moviesData } from '../store/MoviesData';
import { mapMoviesData } from '../helpers/mapMoviesDataHelper';
import { addMovieLocalStorage, deleteMoviesLocalStorage, getMoviesLocalStorage } from '../helpers/localStorageHelper';
import { clearFavoriteListMovies, unmarkHeart } from '../helpers/domHelpers';

export async function searchByName(): Promise<void> {
  let pageCount = 1;
  const URL = `${API_PATH}/search/movie?api_key=${API_KEY}&language=en-US&page=${pageCount}&include_adult=true&query=`;

  const submitButton = <HTMLButtonElement>document.getElementsByClassName('search-button')[0];
  const cardContainerDescriptionList = <HTMLCollection>document.getElementsByClassName('card-body');
  const poster = <HTMLCollection>document.getElementsByTagName('img');
  const overview = <HTMLCollection>document.getElementsByClassName('card-text truncate');
  const releaseDate = <HTMLCollection>document.getElementsByClassName('text-muted');
  const heartIcon = document.getElementsByClassName('bi-heart-fill');

  const loadMoreButton = <HTMLButtonElement>document.getElementById('load-more');

  const randomMovieContainer = <HTMLDivElement>document.getElementById('random-movie');
  const randomMovieTitle = <HTMLDivElement>document.getElementById('random-movie-name');
  const randomMovieTextContainer = <HTMLDivElement>document.getElementById('random-movie-description');
  const randomImage = <HTMLCollection>document.getElementsByClassName('random-image');

  submitButton.addEventListener('click', function(): void {
    clearFavoriteListMovies();
    clearRandomMovie();
    const searchValue = (<HTMLInputElement>document.getElementsByClassName('search-input')[0]).value;
    searchMoviesByTitle(searchValue);
    clearTitle();
    moviesData.clearMoviesData();
    unmarkHeart();
  });

  const searchMoviesByTitle = async (title: string): Promise<void> => {
    const options = {
      method: 'GET',
    };
    await fetch(URL + title, options)
        .then(response => response.json())
        .then(data => {
          const { results } = data;
          if (results !== undefined) {
            mapMoviesData(results);
            showMovieInformation(moviesData.getMoviesData());
            displayRandomMovie(moviesData.getMoviesData());
          }
        })
        .catch(error => {
          console.error(error);
        });
  };

  const displayRandomMovie = (arrayDataMovies: Array<any>): void => {
    const randomElement = arrayDataMovies[Math.floor(Math.random() * arrayDataMovies.length)];

    const img = <HTMLImageElement>document.createElement('img');
    img.setAttribute('src', `${POSTER_URL}${randomElement.backdrop_path}`);
    img.setAttribute('class', 'random-image');
    randomMovieTextContainer.innerHTML = randomElement.overview;
    randomMovieTitle.innerHTML = randomElement.title;
    randomMovieContainer.appendChild(img);
  };

  const clearRandomMovie = () => {
    randomMovieTextContainer.innerHTML = '';
    randomMovieTitle.innerHTML = '';
    for (let i = 0; i < randomImage.length; i++) {
      randomMovieContainer.removeChild(randomImage[i]);
    }
  };

  Array.from(heartIcon).forEach((item: any, index: number): void => {
    item.addEventListener('click', function() {
      const heartStyle = item.getAttribute('fill');
      if (heartStyle === '#ff000078') {
        item.setAttribute('fill', 'red');
        addMovieLocalStorage(moviesData.getMoviesData()[index].id);
      }
      if (heartStyle === 'red') {
        item.setAttribute('fill', '#ff000078');
        deleteMoviesLocalStorage(moviesData.getMoviesData()[index].id);
      }
    });
  });

  const showMovieInformation = (arrayDataMovies: Array<any>): void => {
    const favoriteMoviesId: Array<number> = getMoviesLocalStorage();
    for (let i = 0; i < arrayDataMovies.length; i++) {
      const title = document.createElement('h5');
      title.setAttribute('style', 'text-align: center');
      title.setAttribute('class', 'movie-title');

      if (favoriteMoviesId !== null) {
        const favorite = favoriteMoviesId.filter(item => item === arrayDataMovies[i].id);
        if (favorite.length !== 0) {
          heartIcon[i].setAttribute('fill', 'red');
          console.log(favorite);
          console.log(heartIcon[i]);
        }
      }

      if (arrayDataMovies[i].poster_path === null || arrayDataMovies[i].overview === null || arrayDataMovies[i].release_date === null) {
        poster[i].setAttribute('src', DEFAULT_POSTER_URL);
        overview[i].innerHTML = 'No overview';
        releaseDate[i].innerHTML = 'No data release';
      } else {
        poster[i].setAttribute('src', POSTER_URL + arrayDataMovies[i].poster_path);
        overview[i].innerHTML = arrayDataMovies[i].overview;
        releaseDate[i].innerHTML = arrayDataMovies[i].release_date;
      }

      title.innerHTML = arrayDataMovies[i].title;
      cardContainerDescriptionList[i].appendChild(title);
    }
  };

  const clearTitle = (): void => {
    const clearTitle = document.getElementsByTagName('h5');
    for (let i = 0; i < clearTitle.length; i++) {
      clearTitle[i].innerHTML = '';
    }
  };

  loadMoreButton.addEventListener('click', function() {
    pageCount += 1;
  });
}
