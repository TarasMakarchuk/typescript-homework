import { addMovieLocalStorage, deleteMoviesLocalStorage, getMoviesLocalStorage } from '../helpers/localStorageHelper';
import { mapFavoritesMoviesData } from '../helpers/mapMoviesDataHelper';
import { API_KEY, API_PATH, DEFAULT_POSTER_URL, POSTER_URL } from '../consts/consts';
import { favoriteMoviesData } from '../store/FavoritesData';
import { clearFavoriteListMovies } from '../helpers/domHelpers';

export async function favoriteMovies(): Promise<void> {

  const favoriteMoviesContainer = <HTMLElement>document.getElementById('favorite-movies');
  const favoriteButton = <HTMLButtonElement>document.getElementsByClassName('navbar-toggler')[0];
  const favoritePoster = <HTMLCollection>document.getElementsByClassName('favorite-poster');
  const favoriteOverview = <HTMLCollection>document.getElementsByClassName('favorite-card-text');
  const favoriteReleaseDate = <HTMLCollection>document.getElementsByClassName('favorite-text-muted');
  const favoriteTitle = <HTMLCollection>document.getElementsByClassName('favorite-title');

  function createFavoriteCard() {
    const favoriteContainer = document.createElement('div');
    favoriteContainer.setAttribute('class', 'col-12 p-2');
    favoriteContainer.innerHTML = `
        <div class="col-12 p-2">
          <div class="card shadow-sm">
            <img class="favorite-poster" src="https://image.tmdb.org/t/p/original//rTh4K5uw9HypmpGslcKd4QfHl93.jpg"/>
             <svg xmlns="http://www.w3.org/2000/svg" stroke="red" fill="red" width="50" height="50"
                            class="bi position-absolute p-2 favorite-heart-icon" viewBox="0 -2 18 22">
                <path fill-rule="evenodd"
                      d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" />
             </svg>
             <div class="card-body favorite-card-body">
               <p class="card-text truncate favorite-card-text">
                           askdkasdkasdklas
                           asldk;lasdk;laskd;las
               </p>
               <div class="d-flex justify-content-between align-items-center">
                 <small class="text-muted favorite-text-muted">2021-05-26</small>
               </div>
                  <h5 class="favorite-title" style="text-align: center">Title</h5>
             </div>
           </div>
         </div>`;
    favoriteMoviesContainer.appendChild(favoriteContainer);
  }

  favoriteButton.addEventListener('click', function() {
    clearFavoriteListMovies();
    searchMovies();
    favoriteMoviesData.clearFavoritesMoviesData();
  });

  const searchMovies = () => {
    const favoriteId: Array<number> = getMoviesLocalStorage();
    const options = {
      method: 'GET',
    };
    const urls = [];
    if (favoriteId !== null) {
      for (let i = 0; i < favoriteId.length; i++) {
        const URL_FAVORITE_MOVIES = `${API_PATH}/movie/${favoriteId[i]}?api_key=${API_KEY}&language=en-US`;
        urls.push(fetch(URL_FAVORITE_MOVIES, options)
            .then(response => response.json()));
      }
    }

    Promise.all([...urls])
        .then(results => {
          if (results !== undefined) {
            mapFavoritesMoviesData(results);
            showMovieInformation(favoriteMoviesData.getFavoriteMoviesData());
          }
        });
  };

  const showMovieInformation = (arrayDataMovies: Array<any>): void => {
    const favoriteHeartIcons = <HTMLCollection>document.getElementsByClassName('favorite-heart-icon');

    for (let i = 0; i < arrayDataMovies.length; i++) {
      createFavoriteCard();

      if (arrayDataMovies[i].poster_path === null || arrayDataMovies[i].overview === null || arrayDataMovies[i].release_date === null) {
        favoritePoster[i].setAttribute('src', DEFAULT_POSTER_URL);
        favoriteOverview[i].innerHTML = 'No overview';
        favoriteReleaseDate[i].innerHTML = 'No data release';
        favoriteTitle[i].innerHTML = 'No title';
      } else {
        favoritePoster[i].setAttribute('src', POSTER_URL + arrayDataMovies[i].poster_path);
        favoriteOverview[i].innerHTML = arrayDataMovies[i].overview;
        favoriteReleaseDate[i].innerHTML = arrayDataMovies[i].release_date;
        favoriteTitle[i].innerHTML = arrayDataMovies[i].title;
      }
    }

    Array.from(favoriteHeartIcons).forEach((item: any, index: number): void => {
      item.addEventListener('click', function() {
        console.log('click');
        const heartStyle = item.getAttribute('fill');
        if (heartStyle === '#ff000078') {
          item.setAttribute('fill', 'red');
          addMovieLocalStorage(arrayDataMovies[index].id);
        }
        if (heartStyle === 'red') {
          item.setAttribute('fill', '#ff000078');
          deleteMoviesLocalStorage(arrayDataMovies[index].id);
        }
      });
    });
  };
}