export interface IMovies {
  id: number;
  title: string;
  overview: string;
  release_date: string;
  poster_path: string;
  backdrop_path: string;
}

export class MoviesResponseData implements IMovies {
  public id: number;
  public title: string;
  public overview: string;
  public release_date: string;
  public poster_path: string;
  public backdrop_path: string;

  public constructor(data: IMovies) {
    this.id = data.id;
    this.title = data.title;
    this.overview = data.overview;
    this.release_date = data.release_date;
    this.poster_path = data.poster_path;
    this.backdrop_path = data.backdrop_path;
    if (!this.isValid()) {
      console.error('Invalid parameters of response as null in IMovies interface');
    }
  }

  public isValid(): boolean {
    for (const item in this) {
      if (this[item] === null) {
        return false;
      }
    }
    return true;
  }
}
