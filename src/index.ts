import { searchByName } from './movies/searchByName';
import { categoriesMovies } from './movies/categoriesMovies';
import { favoriteMovies } from './movies/favoriteMovies';

export async function render(): Promise<void> {
    await searchByName();
    await categoriesMovies();
    await favoriteMovies();
}
